import os, zipfile

def backup_to_zip(dir):
    path = os.path.abspath(dir)
    number = 1
    while True:
        zipFileName = os.path.basename(path) + "_" + str(number) + ".zip"
        if not os.path.exists(zipFileName):
            break
        number = number + 1

    backupZip = zipfile.ZipFile(zipFileName, 'w')

    for folderName, subfolders, filenames in os.walk(dir):
        backupZip.write(folderName)
        for filename in filenames:
            backupZip.write(os.path.join(folderName, filename))

    backupZip.close()
    print("Backup completado.")

