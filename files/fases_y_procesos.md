## Definición de análisis forense

La investigación de un sistema informático de manera exhaustiva a raíz de una sospecha, o certeza, de que el sistema ha sido comprometido, o ha sido utilizado para cometer un acto delictivo. Mayormente se aplica a delitos que se cometen a través de la red o a través de la infraestructura de la red.

### Términos clave

- **DFIR**: Equipo de respuesta a incidentes de forma ágil. Las personas de este tipo de equipos se desplazan lo más rápido posible al lugar del incidente para tratar de contener la amenaza, hacer adqusición de evidencias, analizarlas y resolver finalmente el incidente. En este caso importa más resolver el incidente cuanto antes para que afecte lo mínimo posible al funcionamiento de la empresa atacada. Sin embargo, al tener que trabajar en los propios equipos infectados ya no se podrá garantizar la integridad de las evidencias para presentarlas ante un juzgado.
- **Evidencia**: Un elemento que pude guardar información digital y que puede ser útil para resolver un caso (un disco duro, una ram, los logs del firewall)
- **Cadena de custodia**: Hace referencia a un documento en el que se va apuntado, durante la fase de adquisición, toda la información relativa a las evidencias y al propio proceso de adquisición:

  - Detalles del hardware
  - Detalles del software utilizado
  - Quién estaba presente durante la adquisición
  - En lugar físico en el que se realiza la adquisición
  - A donde se van a llevar las evidencias
  - El hash de cada evidencia

### Sobre el propio análisis

- **Objetivos de un análisis**: Responder a las siguientes preguntas
  - Qué hechos han ocurrido?
  - Quién ha realizado los hechos?
  - Cómo se han realizado?
  - Cuándo se han realizado?

- **Consejos**: 
  - Acotar la investigación a un rango temporal, ya que puede haber muchísimos datos y el análisis se puede prolongar demasiado.
  - Definir el alcance de la investigación, pensando en las preguntas que se quieren responder. Para poder tener claro desde un principio como dirigir la investigación.
  - Reconstruir la línea temporal, para poder darle sentido a los datos analizados.
  - Hacer al menos dos copias de las evidencias (discos, ram...), para tener siempre una copia de reserva si se fastidia la primera
  - Utilizar siempre herramientas ligeras para hacer la adquisición de evidencias, para alterar lo menos posible el estado del sistema a analizar. Y nunca instalarlas en el propio equipo, sino que hay que ejecutarlas desde un disco externo. Y lo óptimo sería hacer los clonados a través de la red, para mantener el equipo a analizar sin ningún cambio.
  - Si el equipo está infectado con malware nunca se debe analizar con un antimalware, para así poder tener acceso al propio malware y poder estudiarlo y saber qué hace. Además se recomiendo no subir el malware a VirusTotal en caso de un ataque dirigido en especial a una empresa, sino los atacantes ya sabrían que lo hemos detectado. Lo que si se puede hacer es calcular el hash del malware y mirar si ya existe en VirusTotal.
  - Adquirir tantas evidencias como sea posible, que nunca se sabe lo que puede hacer falta (logs de acceso a la bbdd, logs de firewall, ids, ips, logs del proxy)
  - Documentar TODO. P.ej al hacer el volcado de la ram apuntar la hora a la que se inicia, qué herramienta se utiliza, apuntar si esa herramienta falla, todo lo anterior si se utiliza otra herramienta, hora de finalización del volcado

### Fases de un análisis

- **Identificación y adquisición**

  - Fotografiar la escena, la pantalla del equipo (si está encendido) asegurándose de que se ve bien la fecha y hora

  - **Adquisición de elementos volátiles** (conexiones a red, procesos, usuarios, dlls, ram). Toda esta info se guarda en ficheros en un disco externo, nunca en el propio equipo a analizar

    - Lo primero a recuperar son las conexiones a red, que son las que antes pueden desaparecer

      ``` bash
      date /t > FechaYHoraDeInicio.txt &time /t >> "FechaYHoraDeInicio.txt"
      netstat -an | findstr /i "estado listening established" > "PuertosAbiertos.txt"
      netstat -anob > "AplicacionesConPuertosAbiertos.txt"
      ipconfig /all > "EstadoDeLaRed.txt"
      nbtstat -S > "ConexionesNetBiosEstablecidas.txt"
      net sessions > "SesionesRemotasEstablecidas.txt"
      ipconfig /displaydns > "DnsCache.txt"
      arp -a > "ArpCache.txt"
      ```

    - Luego procesos, servicios y tareas programadas

      ``` bash
      tasklist > "ProcesosEnEjecucion.txt"
      pslist -t > "Procesos.txt"
      listdlls > "dlls.txt"
      handle -a > "Handles.txt"
      sc query > "ServiciosEnEjecucion.txt"
      schtasks > "TareasProgramadas.txt"
      ```

    - Usuarios, portapapeles, histórico de comandos, unidades mapeadas, carpetas compartidas

      ``` bash
      netUsers.exe > "UsuariosActualmenteLogueados.txt"
      netUsers.exe /History > "HistoricoUsuariosLogueados.txt"
      InsideClipboard /saveclp > "Portapapeles.clp"
      doskey /history > "HistoricoCmd.txt"
      net use > "UnidadesMapeadas.txt"
      net share > "CarpetasCompartidas.txt"
      ```

    - Ram (dumpit)

    - En caso de tener que hacer un análisis en vivo y trabajar sobre el propio equipo, nunca se deben utilizar las propias herramientas del sistema operativo ya que muchos malwares son capaces de ocultarse a las herramientas nativas. Es mejor utilizar siempre herramientas de terceros. Hay herramientas de **triaje** que ya se encargan de recuperar todos los elementos clave que suelen ser necesarios para poder resolver el incidente.

  - Adquisición de disco duro. DESCONECTAR EL CABLE y clonar. Tiene que ser una copia idéntica bit a bit al original (si el original tiene un sector defectuoso, la copia también)

    - **Clonadora**. Es muy caro pero muy fiable. Copia por hardware bit a bit con bloqueo de escritura en discon de origen.
    - **Bloqueadora de escritura**. El clonado se hace por software, pero los discos origen se conectan al destino mediante una bloqueadora de escritura (hardware) para garantizar que no se escribe nada en la evidencia.
    - **Clonado simple por software**. El disco origen se conecta directamente al de destino y se utilizan herramientas como dd o dcfldd para hacer el cloando, calcular el hash y demás

  - Copias de pendrives, cds, logs...

- **Preservación**: Se calcula el hash de todas las evidencias para verificar que las copias son idénticas a las originales, para que todo sea válido en un proceso judicial.

- **Análisis**

  - Lo primero que se analiza es el fichero de la ram (volatility), que suele contener la info más importante (procesos, servicios, conexiones activas, ficheros, historial de navegación, emails, pass, comandos ejecutados, elementos ocultos, registro)
    - Aquí también hay que tener en cuenta el **archivo de hibernación del sistema**, que vuelca en un archivo al disco duro el contenido de la ram cuando el equipo se suspende. Y también el archivo de paginación, que carga en disco datos que han estado en la ram pero que ya no se utilizan, para liberar espacio en la ram.

- **Informe**

Video en 58:12 

