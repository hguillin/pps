# This is a sample Python script.
import requests, backup
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def procesar_data(file):
    resultado = None
    if (file != None):
        d = file.text.replace("\n", "|").split('|')
        operador = d[0]
        com1 = d[1]
        com2 = d[2]
        com3 = d[3]
        resultado = operador, com1, com2, com3
    return resultado


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    rq = requests.get('https://gitlab.com/hguillin/pps/-/raw/main/flow.txt')
    data = procesar_data(rq)

    isBackup = data[1].split(':')[1]
    # isSent = data[2].split(':')[1]
    isAuth = data[3].split(':')[1]

    if isBackup == '1' and isAuth == '1':
        backup.backup_to_zip("files")
    else:
        print("El backup no se ha completado por una de las siguientes razones:", "\t-No está activado", "\t-No está autorizado", sep="\n")




